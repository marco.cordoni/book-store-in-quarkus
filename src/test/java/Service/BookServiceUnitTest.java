package Service;

import entity.AuthorEntity;
import entity.BookEntity;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import repository.BookRepository;
import service.BookService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@QuarkusTest
class BookServiceUnitTest {

    @Inject
    private BookService bookService;

    @InjectMock
    private BookRepository bookRepository;

    @BeforeEach
    void setUp() {
        AuthorEntity authorEntity = new AuthorEntity();
        authorEntity.setId(1);
        authorEntity.setName("Licia Troisi");
        authorEntity.setBirthDate(LocalDate.now());

        List<BookEntity> listEntity = new ArrayList<>();

        BookEntity bookEntity1 = new BookEntity();
        bookEntity1.setId(1);
        bookEntity1.setName("La profezia dell'armadillo");
        bookEntity1.setAuthor(authorEntity);
        bookEntity1.setPublicationDate(LocalDate.now());
        bookEntity1.setCreatedAt(LocalDateTime.now());
        bookEntity1.setUpdatedAt(LocalDateTime.now());
        listEntity.add(bookEntity1);

        BookEntity bookEntity2 = new BookEntity();
        bookEntity2.setId(2);
        bookEntity2.setName("Un polpo alla gola");
        bookEntity2.setAuthor(authorEntity);
        bookEntity2.setPublicationDate(LocalDate.now());
        bookEntity2.setCreatedAt(LocalDateTime.now());
        bookEntity2.setUpdatedAt(LocalDateTime.now());
        listEntity.add(bookEntity2);

        BookEntity bookEntity3 = new BookEntity();
        bookEntity3.setId(3);
        bookEntity3.setName("Kobane Calling");
        bookEntity3.setAuthor(authorEntity);
        bookEntity3.setPublicationDate(LocalDate.now());
        bookEntity3.setCreatedAt(LocalDateTime.now());
        bookEntity3.setUpdatedAt(LocalDateTime.now());
        listEntity.add(bookEntity3);

        authorEntity.setBooks(listEntity);

        listEntity.sort(Comparator.comparing(BookEntity::getName));
        when(bookRepository.listAllBooksSorted()).thenReturn(listEntity);
    }

    @Test
    void testFindAll() {
        assertEquals(3, bookService.findAll().size());
    }
}
