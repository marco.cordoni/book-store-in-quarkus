package resource.rest;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response.Status;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@TestMethodOrder(OrderAnnotation.class)
class BookResourceIntegrationTest {

    @Test
    @Order(1)
    void testFindAllBooks() {
        given()
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
        .when()
                .get("/api/books")
        .then()
                .statusCode(Status.OK.getStatusCode())
                .body("size()",is(6));
    }

    @Test
    @Order(2)
    void testCreateABook() {
        String bookDtoJson  = "" +
                "{" +
                "    \"name\": \"Il trono di spade\"," +
                "    \"publicationDate\": \"2009-06-25\"," +
                "    \"author\": {\"id\": 2 }" +
                "}";

        given()
                .body(bookDtoJson)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.WILDCARD)
        .when()
                .post("/api/books")
        .then()
                .statusCode(Status.CREATED.getStatusCode())
                .body("name", is("IL TRONO DI SPADE"));
    }
}
