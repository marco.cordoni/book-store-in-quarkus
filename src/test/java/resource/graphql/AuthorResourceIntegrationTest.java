package resource.graphql;

import dto.AuthorDto;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@TestMethodOrder(OrderAnnotation.class)
class AuthorResourceIntegrationTest {

    @Test
    @Order(1)
    void testFindAllAuthors() {
        String requestBody =    " { \"query\": \" { " +
                                "   allAuthors { " +
                                "       id " +
                                "       name " +
                                "       alias " +
                                "       birthDate " +
                                "       createdAt " +
                                "       updatedAt " +
                                "   } " +
                                " }\" } ";

        Response response =
                given()
                    .contentType(ContentType.JSON)
                    .body(requestBody)
                .when()
                    .post("/graphql")
                .then()
                    .assertThat()
                    .statusCode(RestResponse.Status.OK.getStatusCode())
                    .and()
                    .extract()
                    .response();

        List<AuthorDto> allAuthors = response.jsonPath().getList("data.allAuthors", AuthorDto.class);

        assertThat(allAuthors)
                .isNotEmpty()
                .hasSize(2)
                .extracting(AuthorDto::getName)
                .contains("Michele Rech", "George R. R. Martin");
    }

    @Test
    @Order(2)
    void testSaveAuthor() {
        String requestBody =    " { \"query\": \" { " +
                                "   author(authorId: 1) { " +
                                "       id " +
                                "       name " +
                                "       alias " +
                                "       birthDate " +
                                "       createdAt " +
                                "       updatedAt " +
                                "   } " +
                                " }\" } ";

        Response response =
                given()
                    .contentType(ContentType.JSON)
                    .body(requestBody)
                .when()
                    .post("/graphql")
                .then()
                    .assertThat()
                    .statusCode(RestResponse.Status.OK.getStatusCode())
                    .and()
                    .extract()
                    .response();

        AuthorDto authorDto = response.jsonPath().getObject("data.author", AuthorDto.class);

        assertThat(authorDto)
                .extracting(AuthorDto::getAlias)
                .isEqualTo("Zerocalcare");
    }
}
