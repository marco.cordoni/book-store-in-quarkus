package mapper.author;

import dto.AuthorDto;
import dto.BookDto;
import entity.AuthorEntity;
import entity.BookEntity;
import mapper.UtilsMapper;
import org.mapstruct.*;

import java.util.List;

@Mapper(
        componentModel = "jakarta",
        uses = {UtilsMapper.class},
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public abstract class AuthorMapper {

    // ---------------------- SIMPLE MAPPING ----------------------
    @Named("mapAuthorDtoToEntity")
    public abstract AuthorEntity map(AuthorDto value);

    @Named("mapAuthorEntityToDto")
    public abstract AuthorDto map(AuthorEntity value);

    // ---------------------- MAPPING IGNORING USELESS FIELDS ----------------------
    @Named("mapAuthorEntityToDtoWithoutBooks")
    @Mapping(source = "books", target = "books", ignore = true)
    public abstract AuthorDto mapAuthorEntityToDtoWithoutBooks(AuthorEntity value);


    // ---------------------- MAPPING USING A CUSTOM MAPPER FOR A CERTAIN FIELD ----------------------
    @Named("mapAuthorEntityToDtoSetUnknownAliasIfNull")
    @Mapping(source = "alias", target = "alias", qualifiedByName = "setUnknownIfNull")
    public abstract AuthorDto mapAuthorEntityToDtoSetUnknownAliasIfNull(AuthorEntity author);


    // ---------------------- MAPPING LIST USING A SPECIF MAPPER FOR EACH ELEMENT ----------------------
    @Named("mapEntityListToDtoListWithoutBooks")
    @IterableMapping(qualifiedByName = "mapAuthorEntityToDtoWithoutBooks")
    public abstract List<AuthorDto> mapEntityListToDtoListWithoutBooks(List<AuthorEntity> list);


}
