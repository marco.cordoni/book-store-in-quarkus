package mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Named;

@Mapper(componentModel = "jakarta")
public abstract class UtilsMapper {

    @Named("setUnknownIfNull")
    public String setUnknownIfNull(String s) {
        return s != null ? s : "Unknown";
    }
}
