package application;

import jakarta.ws.rs.core.Application;
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.info.License;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

@OpenAPIDefinition(
        tags = {
                @Tag(name="store", description="Operations to manage a book store."),
                @Tag(name="books", description="Operations related to books")
        },
        info = @Info(
                title="Example API",
                version = "1.0.0",
                contact = @Contact(
                        name = "Book API Support",
                        url = "http://exampleurl.com/contact",
                        email = "marco.cordoni@example.com"
                ),
                license = @License(
                        name = "License MIT",
                        url = "https://en.wikipedia.org/wiki/MIT_License"
                )
        )
)
public class ApiApplication extends Application {
}