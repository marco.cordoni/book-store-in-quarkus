package service;

import annotation.interceptor.logging.generic.Logging;
import dto.AuthorDto;
import dto.BookDto;
import entity.AuthorEntity;
import exception.custom.AuthorNotFoundException;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import mapper.author.AuthorMapper;
import mapper.book.BookMapper;
import org.jetbrains.annotations.NotNull;
import repository.AuthorRepository;

import java.util.List;
import java.util.Optional;

@Logging
@ApplicationScoped
public class AuthorService {

    @Inject
    private AuthorRepository authorRepository;

    @Inject
    private AuthorMapper authorMapper;

    @Inject
    private BookMapper bookMapper;

    public List<AuthorDto> getAllAuthors() {
        List<AuthorEntity> authorEntityList = authorRepository.listAll();
        return authorMapper.mapEntityListToDtoListWithoutBooks(authorEntityList);
    }

    public AuthorDto findById(Integer id, boolean getBooks) {
        AuthorEntity authorEntity = getAuthorEntity(id);

        return getBooks
                ?
                bookMapper.map(authorEntity)
                :
                authorMapper.mapAuthorEntityToDtoWithoutBooks(authorEntity);
    }

    @NotNull
    private AuthorEntity getAuthorEntity(Integer id) {
        Optional<AuthorEntity> authorEntityOpt = authorRepository.findByIdOptional(Long.valueOf(id));

        if (authorEntityOpt.isEmpty()) {
            throw new AuthorNotFoundException("ERROR: an author with id " + id + " does not exists!");
        }
        return authorEntityOpt.get();
    }

    public Integer countBooksByAuthorId(Integer id) {
        return authorRepository.countBooksByAuthorId(id);
    }

    public AuthorDto saveAuthor(AuthorDto authorDto) {

        AuthorEntity authorEntity = null;

        if (authorDto.getId() == null) {
            authorEntity = authorMapper.map(authorDto);
        }
        else {
            authorEntity = getAuthorEntity(authorDto.getId());
            authorEntity.setName(authorDto.getName());
            authorEntity.setBirthDate(authorDto.getBirthDate());
            authorEntity.setAlias(authorDto.getAlias());
        }

        authorRepository.persist(authorEntity);
        return  authorMapper.map(authorEntity);
    }
}
