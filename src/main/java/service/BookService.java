package service;

import annotation.interceptor.logging.generic.Logging;
import dto.BookDto;
import entity.BookEntity;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import mapper.book.BookMapper;
import repository.BookRepository;

import java.util.List;

@Logging
@ApplicationScoped
public class BookService {

    @Inject
    private BookRepository bookRepository;

    @Inject
    private AuthorService authorService;

    @Inject
    private BookMapper bookMapper;


    /**
     * Retrieves all books and related authors.
     * This method retrieves all books sorted by name in ascending order.
     *
     * @return List of books sorted by name in ascending order.
     */
    public List<BookDto> findAll() {
        List<BookEntity> listEntity = bookRepository.listAllBooksSorted();
        return bookMapper.mapEntityListToDtoList(listEntity);
    }

    /**
     * Save a book.
     * This method save a book on the database and return the book just saved.
     *
     * @param   bookDto      the book to save.
     * @return  bookDto      the book just saved.
     * @throws  IllegalArgumentException if the book already has an ID, the ID must be provided by the database.
     */
    public BookDto saveBook(BookDto bookDto) {
        if(bookDto.getId() != null) {
            throw new IllegalArgumentException("The book to save can not have an ID");
        }

        BookEntity bookEntity = bookMapper.map(bookDto);
        bookEntity = bookRepository.persistAndRetrieveNoCache(bookEntity);
        return bookMapper.map(bookEntity);
    }

    /**
     * Get True if a book already exists with that name and author and false otherwise.
     *
     * @return  Boolean     true or false.
     */
    public Boolean existBookNotDeletedWithNameAndAuthor(String name, String author) {
        return bookRepository.existBookNotDeletedWithNameAndAuthor(name, author);
    }

    /**
     * Hard delete a book given its ID.
     *
     * @param   id  the ID of the book to delete.
     */
    public void hardDeleteById(Integer id) {
        bookRepository.deleteById(Long.valueOf(id));
    }

    /**
     * Soft delete a book given its ID.
     *
     * @param   id  the ID of the book to delete.
     */
    public void softDeleteById(Integer id) {
        BookEntity bookEntity = bookRepository.findById(Long.valueOf(id));
        bookEntity.setDeleted(true);
        bookRepository.persist(bookEntity);
    }

    /**
     * Update a book.
     * This method update a book on the database and return the book just updated.
     *
     * @param   bookDto      the book to update.
     * @return  bookDto      the book just updated.
     * @throws  IllegalArgumentException if the book hasn't an ID.
     */
    public BookDto updateBook(BookDto bookDto) {
        if(bookDto.getId() == null) {
            throw new IllegalArgumentException("The book to save must have an ID");
        }

        BookEntity bookEntityToUpdate = bookRepository.findById(Long.valueOf(bookDto.getId()));
        bookEntityToUpdate.setName(bookDto.getName());
        bookEntityToUpdate.setPublicationDate(bookDto.getPublicationDate());

        BookDto bookDtoUpdated = bookMapper.map(bookEntityToUpdate);
        bookDtoUpdated.setBooksOfThisAuthor(authorService.countBooksByAuthorId(bookDtoUpdated.getAuthor().getId()));

        return bookDtoUpdated;
    }
}
