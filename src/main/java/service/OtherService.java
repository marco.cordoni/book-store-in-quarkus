package service;

import annotation.interceptor.logging.generic.Logging;
import dto.JokeDto;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import restclient.RandomJokeClient;

@Logging
@ApplicationScoped
public class OtherService {

    @Inject
    @RestClient
    private RandomJokeClient randomJokeClient;


    /**
     * Get a book.
     * Get random jokes.
     *
     * @return  JokeDto     the joke.
     */
    public JokeDto getJoke() {
        return randomJokeClient.getRandomJoke();
    }
}
