package repository;

import entity.BookEntity;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import io.quarkus.panache.common.Sort;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class BookRepository implements PanacheRepository<BookEntity> {

    /**
     * This method retrieves a list of books sorted by name in ascending order with the related author.
     *
     * @return A list of books sorted by name in ascending order.
     *
     * @see BookEntity
     */
    public List<BookEntity> listAllBooksSorted() {
        return list("deleted = false", Sort.ascending("name"));
    }


    /**
     * This method return true if exist a book not deleted with that name
     * and author and false otherwise.
     *
     * @param name The name of the book.
     * @param author The name of the author.
     * @return True or false.
     */
    public Boolean existBookNotDeletedWithNameAndAuthor(String name, String author) {
        String jpql = " SELECT b " +
                "       FROM BookEntity b " +
                "       WHERE b.deleted = false AND " +
                "           b.name = :name AND " +
                "           b.author.name = :author";

        Parameters parameters = Parameters.with("name", name).and("author", author);

        long bookCounter = find(jpql, parameters).count();
        return bookCounter > 0;
    }


    /**
     * This method save the book and retrieve it avoid using the cache.
     *
     * @param bookEntity The book.
     * @return bookEntity The book saved.
     */
    public BookEntity persistAndRetrieveNoCache(BookEntity bookEntity) {
        persist(bookEntity);
        getEntityManager().clear();
        return findById(Long.valueOf(bookEntity.getId()));
    }

}
