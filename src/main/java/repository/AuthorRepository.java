package repository;

import entity.AuthorEntity;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class AuthorRepository implements PanacheRepository<AuthorEntity> {

    /**
     * This method return the number of books of an author given his id.
     *
     * @param id The ID of the author.
     * @return The number of books of the author.
     */
    public Integer countBooksByAuthorId(Integer id) {
        return findById(Long.valueOf(id)).getBooks().size();
    }
}
