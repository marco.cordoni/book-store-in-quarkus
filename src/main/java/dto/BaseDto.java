package dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@RegisterForReflection
public class BaseDto {
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
