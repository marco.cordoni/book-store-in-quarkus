package dto;

import io.quarkus.runtime.annotations.RegisterForReflection;
import jakarta.batch.runtime.JobExecution;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Properties;

@Getter
@Setter
@RegisterForReflection
public class JobDataDto {
    private String status;
    private Long executionId;
    private String jobName;
    private String batchStatus;
    private Date startTime;
    private Properties jobParameters;

    public JobDataDto() {
    }

    public JobDataDto(final Long executionId, final String status) {
        this.executionId = executionId;
        this.status = status;
    }

    public JobDataDto(JobExecution jobExecution) {
        this.executionId = jobExecution.getExecutionId();
        this.jobName = jobExecution.getJobName();
        this.batchStatus = jobExecution.getBatchStatus().toString();
        this.startTime = jobExecution.getStartTime();
        this.jobParameters = jobExecution.getJobParameters();
    }
}