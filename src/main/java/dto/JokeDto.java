package dto;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
@RegisterForReflection
public class JokeDto {

    private Integer id;
    private String type;
    private String setup;
    private String punchline;
    private Date dateDownload;
}
