package annotation.validator.book;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import repository.BookRepository;

@ApplicationScoped
public class ExistingBookIdConstraint implements ConstraintValidator<ExistingBookId, Integer> {

    @Inject
    private BookRepository bookRepository;

    @Override
    public boolean isValid(Integer bookId, ConstraintValidatorContext context) {
        return bookRepository.findByIdOptional(Long.valueOf(bookId)).isPresent();
    }
}
