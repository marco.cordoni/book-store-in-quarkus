package annotation.validator.book;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

/**
 * The ValidBook annotation is used to validate the content of the book before save it.
 *
 * Its implementation is in the class BookValidator.
 *
 * If the validation fail an error is returned with the message
 * "A book with this name for this author already exist!".
 */
@Documented
@Constraint(validatedBy = BookValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidBook {

    String message() default "Validation failed!";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
