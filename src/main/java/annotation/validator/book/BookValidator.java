package annotation.validator.book;

import dto.AuthorDto;
import dto.BookDto;
import jakarta.inject.Inject;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.jboss.logging.Logger;
import repository.BookRepository;
import service.AuthorService;
import service.BookService;

public class BookValidator implements ConstraintValidator<ValidBook, BookDto> {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Inject
    private BookRepository bookRepository;

    @Inject
    private BookService bookService;

    @Inject
    private AuthorService authorService;

    @Override
    public void initialize(ValidBook constraintAnnotation) {
        // Metodo necessario per l'inizializzazione
    }

    /**
     * This method checked that does not already exist a book with the same name for the same author.
     * @param bookDto the BookDto
     * @param context context in which the constraint is evaluated
     *
     * @return a Boolean, true if it does not exit a book with the same name for the same author.
     */
    @Override
    public boolean isValid(BookDto bookDto, ConstraintValidatorContext context) {
        logger.info("Validate the book");

        // If the book hasn't and ID it have to be saved, check if it is not duplicated
        if (bookDto.getId() == null) {
            AuthorDto authorDto = authorService.findById(bookDto.getAuthor().getId(), false);
            boolean checkPassed = !bookService.existBookNotDeletedWithNameAndAuthor(bookDto.getName(), authorDto.getName());

            if (!checkPassed) {
                context.buildConstraintViolationWithTemplate("A book with this name for this author already exist!")
                        .addConstraintViolation();
            }

            return checkPassed;
        }
        // If the book has an ID, check if it really exists
        else {
            boolean checkPassed = bookRepository.findByIdOptional(Long.valueOf(bookDto.getId())).isPresent();

            if (!checkPassed) {
                context.buildConstraintViolationWithTemplate("A book with this ID doesn't exist!")
                        .addPropertyNode("id")
                        .addConstraintViolation();
            }

            return checkPassed;
        }
    }
}
