package annotation.validator.book;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@ApplicationScoped
@Constraint(validatedBy = ExistingBookIdConstraint.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.FIELD})
public @interface ExistingBookId {

    /**
     * @return the error message template
     */
    public String message() default "The book ID does not exist!";

    /**
     * @return the groups the constraint belongs to
     */
    public Class<?>[] groups() default {};

    /**
     * @return the payload associated to the constraint
     */
    public Class<? extends Payload>[] payload() default {};

}
