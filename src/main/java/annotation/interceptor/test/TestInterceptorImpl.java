package annotation.interceptor.test;

import jakarta.annotation.Priority;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import jakarta.ws.rs.core.Response;
import org.jboss.logging.Logger;

/**
 * I've made this annotation to learn how to do operations before and after
 * the method on which this annotation is placed.
 */
@TestInterceptor
@Priority(5) // priority, low = more priority
@Interceptor
public class TestInterceptorImpl {

    private final Logger log = Logger.getLogger(this.getClass());

    /**
     * This method is called every time a method is invoked in the target class.
     * It logs before and after the method invocation and the response of the method.
     *
     * @param context the InvocationContext object which contains the context information
     * for the invoked method.
     *
     * @return Object the result of the invoked method.
     *
     * @throws Exception if an exception is thrown by the method invocation.
     */
    @AroundInvoke
    Object testInvocation(InvocationContext context) throws Exception {
        String className = context.getTarget().getClass().getName(); /* Get the class name of the logged resource */
        String methodName = context.getMethod().getName();           /* Get the method name of the logged resource */

        log.info("BEFORE: " + methodName + " on class: " + className);

        Response returnedObj = (Response) context.proceed();
        log.info("RESPONSE: " + returnedObj.getEntity().toString());
        log.info("RESPONSE: " + returnedObj.getStatus());

        log.info("AFTER: " + methodName + " on class: " + className);

        return returnedObj;
    }
}