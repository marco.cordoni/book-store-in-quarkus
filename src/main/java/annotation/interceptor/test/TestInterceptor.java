package annotation.interceptor.test;

import jakarta.interceptor.InterceptorBinding;

import java.lang.annotation.*;

/**
 * I've made this annotation to learn how to do operations before and after
 * the method on which this annotation is placed.
 */
@InterceptorBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface TestInterceptor {
}