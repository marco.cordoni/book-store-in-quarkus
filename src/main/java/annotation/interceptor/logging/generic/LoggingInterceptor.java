package annotation.interceptor.logging.generic;

import jakarta.annotation.Priority;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import org.jboss.logging.Logger;

/**
 * LoggingInterceptor is a class for intercepting method invocations
 * and logging them.
 *
 * It uses the Logger class from org.jboss.logging.Logger to generate the log messages.
 *
 * The class is annotated with @Logging, @Priority, and @Interceptor
 * to denote that it is an interceptor and the priority level at which it should be executed.
 *
 * The LoggingInterceptor is used in conjunction with the @AroundInvoke annotation
 * to denote that this interceptor should be called whenever a method in the target
 * class is invoked.
 */
@Logging
@Priority(10) // priority, low = more priority
@Interceptor
public class LoggingInterceptor {

    private final Logger log = Logger.getLogger(this.getClass());

    /**
     * This method is called every time a method is invoked in the target class.
     * It logs the method name and class name of the invoked method.
     *
     * @param context the InvocationContext object which contains the context information
     * for the invoked method.
     *
     * @return Object the result of the invoked method.
     *
     * @throws Exception if an exception is thrown by the method invocation.
     */
    @AroundInvoke
    Object logInvocation(InvocationContext context) throws Exception {
        /* Get the class name of the logged resource */
        String className = context.getTarget().getClass().getName();
        /* Get the method name of the logged resource */
        String methodName = context.getMethod().getName();
        /* Log the method name of the logged resource */
        log.info("Calling method: " + methodName + " on class: " + className);
        return context.proceed();
    }
}