package annotation.interceptor.logging.generic;

import jakarta.interceptor.InterceptorBinding;

import java.lang.annotation.*;

/**
 * The Logging annotation is an interceptor binding annotation used to denote methods, classes,
 * constructors, or fields that should be intercepted by the LoggingInterceptor.
 *
 * This annotation is inherited, meaning that if a class is annotated with @Logging,
 * its subclasses will also be considered to be annotated with @Logging.
 *
 * It can be used on the type level (classes and interfaces), method level, constructor level,
 * and field level.
 *
 * The annotation has runtime retention, meaning it is available to the JVM at runtime,
 * which is necessary for the CDI container to process the annotation.
 */
@InterceptorBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.FIELD})
@Inherited
public @interface Logging {
}