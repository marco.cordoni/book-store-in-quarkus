package annotation.interceptor.logging.exception;

import jakarta.interceptor.InterceptorBinding;

import java.lang.annotation.*;

/**
 * The LoggingException annotation is an interceptor binding annotation used to denote methods
 * or classes, that should be intercepted by the LoggingExceptionInterceptor.
 *
 * This annotation log the message of the exception risen.
 *
 * The annotation has runtime retention, meaning it is available to the JVM at runtime,
 * which is necessary for the CDI container to process the annotation.
 */
@InterceptorBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.FIELD})
public @interface LoggingException {
}