package annotation.interceptor.logging.exception;

import jakarta.annotation.Priority;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import org.jboss.logging.Logger;

/**
 * LoggingExceptionInterceptor is a class for intercepting method invocations
 * in the class ExceptionMappers.
 *
 * It uses the Logger class from org.jboss.logging.Logger to generate the log messages.
 *
 * The class is annotated with @Logging, @Priority, and @Interceptor
 * to denote that it is an interceptor and the priority level at which it should be executed.
 *
 * The LoggingInterceptor is used in conjunction with the @AroundInvoke annotation
 * to denote that this interceptor should be called whenever a method in the target
 * class is invoked.
 */
@LoggingException
@Priority(10) // priority, low = more priority
@Interceptor
public class LoggingExceptionInterceptor {

    private final Logger logger = Logger.getLogger(this.getClass());


    /**
     * This method is called every time a method is invoked in the ExceptionMappers class.
     * It logs the error message of the exception catched.
     *
     * @param context the InvocationContext object which contains the context information
     * for the invoked method.
     *
     * @return Object the result of the invoked method.
     *
     * @throws Exception if an exception is thrown by the method invocation.
     */
    @AroundInvoke
    Object logInvocation(InvocationContext context) throws Exception {
        RuntimeException ex = (RuntimeException) context.getParameters()[0];
        String message = ex.getCause() != null ? ex.getCause().getMessage() : ex.getMessage();
        logger.errorf("An error occurred: %s", message);
        return context.proceed();
    }
}