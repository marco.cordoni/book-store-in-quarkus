package resource.graphql;

import dto.AuthorDto;
import io.smallrye.graphql.api.Subscription;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.operators.multi.processors.BroadcastProcessor;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.eclipse.microprofile.graphql.*;
import service.AuthorService;

import java.util.List;

import static jakarta.transaction.Transactional.TxType.REQUIRED;

@GraphQLApi
@ApplicationScoped
@Transactional(REQUIRED)
public class AuthorResource {

    @Inject
    private AuthorService authorService;

    private final BroadcastProcessor<AuthorDto> processorAuthor = BroadcastProcessor.create();

    //  QUERIES
    @Query("allAuthors")
    @Description("Get all Authors")
    public List<AuthorDto> getAllAuthors() {
        return authorService.getAllAuthors();
    }

    @Query
    @Description("Get an Author by his ID")
    public AuthorDto getAuthor(@Name("authorId") int id) {
        return authorService.findById(id, true);
    }

    // MUTATIONS
    @Mutation
    public AuthorDto createOrUpdateAuthor(AuthorDto authorDto) {
        authorDto = authorService.saveAuthor(authorDto);
        processorAuthor.onNext(authorDto);
        return authorDto;
    }

    // SUBSCRIPTION
    @Subscription
    public Multi<AuthorDto> authorCreated(){
        return processorAuthor;
    }
}
