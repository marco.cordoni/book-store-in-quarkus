package resource.rest;

import annotation.validator.book.ExistingBookId;
import dto.BookDto;
import annotation.interceptor.logging.generic.Logging;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.faulttolerance.Fallback;
import org.eclipse.microprofile.faulttolerance.Retry;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.logging.Logger;
import service.BookService;

import java.net.ConnectException;
import java.util.List;

import static jakarta.transaction.Transactional.TxType.REQUIRED;

// Swagger: http://localhost:8080/q/swagger-ui/
// Dev UI: http://localhost:8080/q/dev-v1/

@Tag(name = "Book REST endpoint")
@Path("/api/books")
@Logging
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
@Transactional(REQUIRED)
public class BookResource {

    @Inject
    private BookService bookService;

    @Inject
    private Logger logger;

    @ConfigProperty(name = "env", defaultValue = "unknown")
    private String env;

    @GET
    @Operation(
            summary = "Get all books and related author",
            description = "The book are sorted by name in ascending order"
    )
    @APIResponse(
            responseCode = "200",
            description = "OK",
            content = @Content(
                    mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(
                            type = SchemaType.ARRAY,
                            implementation = BookDto.class
                    )
            )
    )
    public Response findAll() {
        List<BookDto> bookDtoList = bookService.findAll();
        logger.infof("[%s] - Books counter: %s", env, bookDtoList.size());
        return Response.status(Response.Status.OK).entity(bookDtoList).build();
    }

    @POST
    @Retry(delay = 1000, maxRetries = 3, retryOn = {ConnectException.class})
    @Fallback(fallbackMethod = "fallbackOnSavingABook", applyOn = {ConnectException.class})
    @Operation(
            summary = "Insert book",
            description = "Insert a new book"
    )
    @APIResponse(
            responseCode = "201",
            description = "Book Saved",
            content = @Content(
                    mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(
                            type = SchemaType.OBJECT,
                            implementation = BookDto.class
                    )
            )
    )
    public Response saveBook(@Valid @RequestBody BookDto bookDtoToSave) {
        BookDto bookDtoSaved = bookService.saveBook(bookDtoToSave);
        return Response.status(Response.Status.CREATED).entity(bookDtoSaved).build();
    }

    public Response fallbackOnSavingABook(BookDto bookDtoToSave) {
        logger.infof("[%s] - FALLBACK on creating the book %s", env, bookDtoToSave);
        logger.infof("[%s] - SEND MAIL WITH ERROR TO THE APPLICATION MANAGER AND MANAGE THE ERROR", env);
        return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();
    }

    @DELETE
    @Path("/{id}")
    @Operation(
            summary = "Hard delete a book",
            description = "Hard delete a book given its ID"
    )
    @APIResponse(
            responseCode = "200",
            description = "OK"
    )
    public Response hardDeleteById(@PathParam(value = "id") @NotNull @Positive @ExistingBookId Integer id) {
        bookService.hardDeleteById(id);
        return Response.status(Response.Status.OK).build();
    }

    @DELETE
    @Path("/softDelete/{id}")
    @Operation(
            summary = "Soft delete a book",
            description = "Soft delete a book given its ID"
    )
    @APIResponse(
            responseCode = "200",
            description = "OK"
    )
    public Response softDeleteById(@PathParam(value = "id") @NotNull @Positive @ExistingBookId Integer id) {
        bookService.softDeleteById(id);
        return Response.status(Response.Status.OK).build();
    }

    @PUT
    @Operation(
            summary = "Update a book",
            description = "Update an existing book"
    )
    @APIResponse(
            responseCode = "200",
            description = "Book Updated",
            content = @Content(
                    mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(
                            type = SchemaType.OBJECT,
                            implementation = BookDto.class
                    )
            )
    )
    public Response updateBook(@Valid @RequestBody BookDto bookDtoToUpdate) {
        BookDto bookDtoUpdated = bookService.updateBook(bookDtoToUpdate);
        return Response.status(Response.Status.CREATED).entity(bookDtoUpdated).build();
    }

}
