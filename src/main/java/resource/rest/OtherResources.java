package resource.rest;

import annotation.interceptor.logging.generic.Logging;
import annotation.interceptor.test.TestInterceptor;
import dto.JokeDto;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import service.OtherService;

@Tag(name = "Other REST endpoint")
@Path("/api/other")
@Logging
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class OtherResources {

    @Inject
    private OtherService otherService;

    @GET
    @Path("/get/joke")
    @TestInterceptor
    @Operation(
            summary = "Get a joke",
            description = "Get a random joke"
    )
    @APIResponse(
            responseCode = "200",
            description = "OK",
            content = @Content(
                    mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(
                            type = SchemaType.OBJECT,
                            implementation = JokeDto.class
                    )
            )
    )
    public Response getJoke() {
        JokeDto joke = otherService.getJoke();
        return Response.status(Response.Status.OK).entity(joke).build();
    }
}
