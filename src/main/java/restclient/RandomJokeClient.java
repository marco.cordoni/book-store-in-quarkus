package restclient;

import dto.JokeDto;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.faulttolerance.CircuitBreaker;
import org.eclipse.microprofile.faulttolerance.Retry;
import org.eclipse.microprofile.faulttolerance.Timeout;
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import restclient.exception.ApiExceptionMapper;

@ApplicationScoped
@RegisterProvider(ApiExceptionMapper.class)
@RegisterRestClient(configKey = "public.random.joke")
@Path("/")
public interface RandomJokeClient {

    @GET
    @Path("/random_joke")
    @CircuitBreaker(failureRatio=0.75, delay = 1000 ) // the @CircuitBreaker annotation is the @Retry and @Fallback annotations together .
    @Timeout(1000)
    @Retry(maxRetries = 4, delay = 100)
    @Produces({MediaType.APPLICATION_JSON})
    public JokeDto getRandomJoke();

}
