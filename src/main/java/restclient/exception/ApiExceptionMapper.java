package restclient.exception;

import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.rest.client.ext.ResponseExceptionMapper;
import org.jboss.logging.Logger;

/**
 * This class maps an API exception to a RuntimeException.
 * It implements the ResponseExceptionMapper interface from the Eclipse MicroProfile Rest Client API.
 * When a Response is returned with an unsuccessful status code, this class can translate that Response into a RuntimeException.
 */
public class ApiExceptionMapper implements ResponseExceptionMapper<ApiException> {
    private final Logger logger = Logger.getLogger(this.getClass());

    /**
     * This method is used to map the server response to a runtime exception.
     * If the HTTP status code is 401 (Unauthorized) or 422 (Unprocessable Entity),
     * it changes the status code to 400 (Bad Request) and customizes the error message.
     * It logs the error message and the HTTP status code.
     * Finally, it throws a ApiException with the error message and the status code.
     *
     * @param response The server response that needs to be mapped to an exception
     * @return a new instance of return with the message and the status code
     */
    @Override
    public ApiException toThrowable(Response response) {
        String message = response.readEntity(String.class);
        Integer status = response.getStatus();

        logger.error("API ERROR: HTTP Status: " + status);

        return new ApiException(message, status);
    }

}
