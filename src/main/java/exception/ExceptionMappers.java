package exception;

import annotation.interceptor.logging.exception.LoggingException;
import io.vertx.core.http.HttpServerRequest;
import jakarta.ws.rs.core.Response;
import org.jboss.resteasy.reactive.server.ServerExceptionMapper;
import restclient.exception.ApiException;

@LoggingException
class ExceptionMappers {

    /**
     * Handle IllegalArgumentException.
     *
     * @param ex the IllegalArgumentException
     * @param req the HttpServerRequest
     * @return the Response with status BAD_REQUEST
     */
    @ServerExceptionMapper(IllegalArgumentException.class)
    public Response handleIllegalArgumentException(IllegalArgumentException ex, HttpServerRequest req) {
        return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
    }


    /**
     * Handle ApiException.
     *
     * @param ex the ApiException
     * @param req the HttpServerRequest
     * @return the Response with status SERVICE_UNAVAILABLE
     */
    @ServerExceptionMapper(ApiException.class)
    public Response handleProcessingException(ApiException ex, HttpServerRequest req) {
        return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(ex.getMessage()).build();
    }


        // NON SERVE, LASCIARE CHE LE VALIDATION EXCEPTION VENGANO RESTITUITE DA SOLE AL CLIENT
//    /**
//     * Handle ValidationException.
//     *
//     * @param ex the ValidationException
//     * @param req the HttpServerRequest
//     * @return the Response with status BAD_REQUEST
//     */
//    @ServerExceptionMapper(ValidationException.class)
//    public Response handleValidationException(ValidationException ex, HttpServerRequest req) {
////        String message = ex.getCause() != null ? ex.getCause().getMessage() : ex.getMessage();
//        return Response.status(Response.Status.BAD_REQUEST).entity(ex).build();
//    }
}
