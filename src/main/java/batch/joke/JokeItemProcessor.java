package batch.joke;

import java.time.LocalDate;
import java.util.Date;

import dto.JokeDto;
import jakarta.batch.api.chunk.ItemProcessor;
import jakarta.enterprise.context.Dependent;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import org.jboss.logging.Logger;

@Dependent
@Named("jokeItemProcessor")
public class JokeItemProcessor implements ItemProcessor {

    @Inject
    private Logger logger;

    @Override
    public Object processItem(Object item) {
        logger.info("Processing item");

        JokeDto jokeDto = (JokeDto) item;
        jokeDto.setDateDownload(new Date());
        return jokeDto;
    }
}