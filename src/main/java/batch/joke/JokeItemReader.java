package batch.joke;

import java.io.Serializable;

import dto.JokeDto;
import jakarta.batch.api.chunk.ItemReader;
import jakarta.enterprise.context.Dependent;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.transaction.Transactional;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;
import service.OtherService;

@Dependent
@Named("jokeItemReader")
@Transactional
public class JokeItemReader implements ItemReader  {

    @Inject
    private Logger logger;

    @Inject
    private OtherService otherService;

    @Inject
    @ConfigProperty(name = "batch.joke.numJokesToRead", defaultValue = "3")
    private int numJokesToRead;

    @Override
    public void open(Serializable serializable) throws Exception {
        logger.info("Open connection to source");
    }

    @Override
    public void close() throws Exception {
        logger.info("Close connection to source");
    }

    @Override
    public Object readItem() throws Exception {
        JokeDto jokeDto;

        if (numJokesToRead > 0) {
            logger.info("Read item from source");
            jokeDto = getJokeFromExterlanSource();
        }
        else {
            logger.info("Stop read sending a null value");
            jokeDto = null;
        }

        return jokeDto;
    }

    private JokeDto getJokeFromExterlanSource() {
        numJokesToRead--;
        return otherService.getJoke();
    }

    @Override
    public Serializable checkpointInfo() throws Exception {
        return null;
    }
}