package batch.joke;

import dto.JobDataDto;
import jakarta.batch.operations.JobOperator;
import jakarta.batch.runtime.BatchRuntime;
import jakarta.batch.runtime.JobExecution;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

import java.util.Properties;

@Path("/batch/joke")
@Produces(MediaType.APPLICATION_JSON)
public class JokeResource {

    @Inject
    private JobOperator jobOperator;

    @GET
    @Path("/job/execute/")
    @Operation(
            summary = "Start joke batch",
            description = "This API start the batch, this batch read N jokes from an external endpoint and print the on the console."
    )
    @APIResponse(
            responseCode = "200",
            description = "OK",
            content = @Content(
                    mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(
                            type = SchemaType.OBJECT,
                            implementation = JobDataDto.class
                    )
            )
    )
    public Response executeJob() {
        long executionId = jobOperator.start("joke", new Properties());
        JobExecution jobExecution = jobOperator.getJobExecution(executionId);
        return Response.ok(new JobDataDto(jobExecution)).build();
    }

}