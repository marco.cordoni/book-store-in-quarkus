package batch.joke;

import java.util.List;

import dto.JokeDto;
import jakarta.batch.api.chunk.AbstractItemWriter;
import jakarta.enterprise.context.Dependent;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import org.jboss.logging.Logger;

@Dependent
@Named("jokeItemWriter")
public class JokeItemWriter extends AbstractItemWriter {

    @Inject
    private Logger logger;

    @Override
    public void writeItems(List<Object> items) {
        logger.infof("START writing %s items", items.size());

        for (Object item: items) {
            JokeDto jokeDto = (JokeDto) item;
            System.out.println(jokeDto.toString());
        }
    }
}