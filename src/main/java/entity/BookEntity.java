package entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table(name = "books")
@Getter
@Setter
public class BookEntity extends BaseEntity {

    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "id_author", nullable = false)
    private AuthorEntity author;

    @Column(name = "publication_date")
    private LocalDate publicationDate;

    @Column(name = "deleted")
    private boolean deleted;

}
